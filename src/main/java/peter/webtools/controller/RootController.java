package peter.webtools.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import tutorial.MyMessage;

/**
 * 不需要继承任何累，真是怪异！
 */
@Controller
@RequestMapping("/")
public class RootController {
    /**
     * 体验一下简单的spring-mvc.
     * 用到的注释：
     *    RequestMapping
     */
    @RequestMapping(value = "/index", method = RequestMethod.GET)
    public String index(Model model) {
        model.addAttribute("name", "User");
        System.out.println("YASNDSFSD");
        return "index";
    }

    @RequestMapping(value = "/test", method = RequestMethod.GET)
    public String test(Model model) {
        System.out.println("==============================");
        model.addAttribute("name", "User");
        MyMessage.UserInfo.Builder msg = MyMessage.UserInfo.newBuilder();
        msg.setAcctID(32);
        msg.setName("YanDong");
        msg.setStatus(MyMessage.UserStatus.ONLINE);
        MyMessage.UserInfo myMsg = msg.build();
        System.out.println(myMsg.toString());
        System.out.println("==============================");
        return "index";
    }
}
