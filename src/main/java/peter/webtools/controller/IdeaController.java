package peter.webtools.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
//import org.springframework.web.bind.annotation.AjaxResponseBody;

import java.util.List;

import peter.webtools.beans.Idea;
import peter.webtools.dao.IdeaDao;

/**
 * 不需要继承任何累，真是怪异！
 */
@Controller
@RequestMapping("/idea")
public class IdeaController {
    @Autowired
    private IdeaDao dao;

    /**
     * 体验一下简单的spring-mvc.
     * 用到的注释：
     *    RequestMapping
     */
    @RequestMapping(value = "/index", method = RequestMethod.GET)
    public String hello(Model model) {
        model.addAttribute("greeting", "Hello Spring MVC");
        System.out.println("这是Idea");
        return "idea/index";
    }

    /**
     * 获取Idea列表
     */
    @ResponseBody
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public List<Idea> getList() {// , @PathVariable("page") int page
        // public List<Idea> getList(Model model, @RequestParam(value = "page", defaultValue="0") int page) {
        System.out.println(">>> get list");
        int pageNumber = 10;
        int page = 0;
        return dao.getIdeas(page * pageNumber, pageNumber);
    }

    /**
     * 进行一些页面测试
     * 用到的注释：
     *    RequestMapping
     */
    @RequestMapping(value = "/test", method = RequestMethod.GET)
    public String test(Model model) {
        model.addAttribute("greeting", "Hello Spring MVC");
        System.out.println(">>>> 这是");
        return "test";
    }
}
