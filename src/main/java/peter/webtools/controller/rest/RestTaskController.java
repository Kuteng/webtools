package peter.webtools.controller.rest;

import java.util.concurrent.atomic.AtomicLong;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.http.HttpStatus;
import peter.webtools.beans.Task;
import peter.webtools.beans.MyBean;
import peter.webtools.test.DerbyTest;
import peter.webtools.dao.TaskDao;
import java.util.List;
import java.util.Date;
import java.util.Arrays;
import java.sql.Timestamp;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import org.codehaus.jackson.map.ObjectMapper;
import java.io.IOException;

@RestController
@RequestMapping("/rest/task")
public class RestTaskController {
    // 为了声称ID，counter.incrementAndGet()
    private final AtomicLong counter = new AtomicLong();
    @Autowired
    private TaskDao dao;

    /**
     * 得到当前正在进行的任务。
     */
    @RequestMapping(value = "/current")
    public Task requestCurrentTask() {
        Task task = dao.current();
        return task;
    }

    @RequestMapping(value = "/get/{id}")
    public Task requestTask(@PathVariable("id") int id) {
        Task task = dao.getTask(id);
        return task;
    }

    @RequestMapping(value = "/create/{name}")
    public String createTask(@PathVariable("name") String name) {
        dao.endTask();
        dao.createTask(name);
        return "OK";
    }

    @RequestMapping("/create")
    public Task createTask(String name, Integer classId, String[] tags, String details) {
        Task task = new Task();
        task.setName(name);
        task.setClassId(classId);
        task.setTagsByArray(tags);
        task.setDetails(details);
        return task;
    }

    @RequestMapping(value = "/restart/{id}")
    public String createTask(@PathVariable("id") int id) {
        dao.copyTask(id);
        return "OK";
    }

    @RequestMapping(value = "/gets")
    public List<Task> requestTasks() {
        return dao.getTasks();
    }

    @RequestMapping(value = "/stop")
    public String stopTask() {
        dao.endTask();
        return "OK";
    }

    @RequestMapping(value = "/stop/{id}")
    public String stopTask(@PathVariable("id") int id) {
        dao.endTask(id);
        return "OK";
    }

    @RequestMapping(value = "/test")
    public Task test() {
        String taskJson = "{ \"name\" : \"yandong\", \"tags\" : [\"ming\"], \"details\" : \"wodemingzi\", \"startTime\": " + System.currentTimeMillis() + " }";

        try {
            ObjectMapper objectMapper = new ObjectMapper();

            Task task = objectMapper.readValue(taskJson, Task.class);
            System.out.println(task);
            return task;
        }
        catch(IOException e) {
            e.printStackTrace();
        }

        return null;
    }
}
