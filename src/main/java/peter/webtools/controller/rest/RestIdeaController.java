/*************************************************************************
	> File Name: RestIdeaController.java
	> Author: 闫冬
	> Mail: yandongdedipan@163.com
	> Created Time: 2016年10月11日 星期二 17时39分13秒
 ************************************************************************/

package peter.webtools.controller.rest;

import java.util.concurrent.atomic.AtomicLong;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.http.HttpStatus;
import peter.webtools.beans.MyBean;
import peter.webtools.dao.IdeaDao;
import peter.webtools.beans.Idea;
import java.util.List;

@RestController
@RequestMapping("/rest/idea")
public class RestIdeaController {
    // 为了声称ID，counter.incrementAndGet()
	private final AtomicLong counter = new AtomicLong();
    @Autowired
    private IdeaDao dao;

    /**
     * 得到当前正在进行的任务。
     */
	@RequestMapping(value = "/current")
    public List<Idea> requestCurrentIdeas() {
        return dao.getIdeas();
    }

    /**
     * 得到最新的特定数量的ideas。
     */
	@RequestMapping(value = "/list/{number}")
    public List<Idea> requestList(@PathVariable("number") int number) {
        return dao.getIdeas(0, number);
    }

	@RequestMapping(value = "/create/{msg}/{tags}")
    public String createIdea(@PathVariable("msg") String msg, @PathVariable("tags") String tags) {
        dao.addIdea(msg, tags);
        return "OK";
    }

	@RequestMapping(value = "/create/{msg}")
    public String createIdea(@PathVariable("msg") String msg) {
        dao.addIdea(msg, null);
        return "OK";
    }

	@RequestMapping(value = "/create", method = RequestMethod.POST)
    public String createIdeaByPost(String msg, String tags) {
        if(tags == "") {
            tags = null;
        }

        dao.addIdea(msg, tags);
        return "OK";
    }
}
