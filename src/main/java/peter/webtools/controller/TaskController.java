package peter.webtools.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 不需要继承任何累，真是怪异！
 */
@Controller
@RequestMapping("/task")
public class TaskController {
    /**
     * 体验一下简单的spring-mvc.
     * 用到的注释：
     *    RequestMapping
     */
    @RequestMapping(value = "/test", method = RequestMethod.GET)
    public String hello(Model model) {
        model.addAttribute("greeting", "Hello Spring MVC");
        System.out.println("YASNDSFSD");
        return "task/current";
        // return "task/current";
    }

    /**
     * 体验一下读取参数甚至于自定义规范模版的spring-mvc。
     * 用到的注释：
     *    RequestMapping
     *    RequestParam
     *    PathVariable
     */
    @RequestMapping("/userinfo/test/{id}/{language}/{name}")
    public String userInfo(Model model, @PathVariable(value = "id") int id,
            @PathVariable(value = "language") String language, @PathVariable(value = "name") String name,
            @RequestParam(value = "age", defaultValue="8869") String age) {
        model.addAttribute("id", id + 2);
        model.addAttribute("language", language);
        model.addAttribute("name", name);
        model.addAttribute("age", age);
        return "helloworld";
    }
}
