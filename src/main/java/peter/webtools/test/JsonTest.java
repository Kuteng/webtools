package peter.webtools.test;

import peter.webtools.beans.Task;
import peter.webtools.beans.MyBean;
import peter.webtools.test.DerbyTest;
import peter.webtools.dao.TaskDao;
import java.util.List;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import org.codehaus.jackson.map.ObjectMapper;
import java.io.IOException;

public class JsonTest {
    public String test1() {
        String taskJson = "{ \"name\" : \"yandong\", \"tags\" : \"ming\", \"details\" : \"wodemingzi\" }";

        try {
            JsonFactory factory = new JsonFactory();
            JsonParser  parser  = factory.createParser(taskJson);
            while(!parser.isClosed()) {
                JsonToken jsonToken = parser.nextToken();

                if(JsonToken.FIELD_NAME.equals(jsonToken)){
                    String fieldName = parser.getCurrentName();

                    jsonToken = parser.nextToken();

                    System.out.println(fieldName + ": " + parser.getValueAsString());
                }
                
            }
        }
        catch(IOException e) {
            e.printStackTrace();
        }

        return "OK";
    }

    public Task test() {
        String taskJson = "{ \"name\" : \"yandong\", \"tags\" : [\"ming\"], \"details\" : \"wodemingzi\", \"startTime\": " + System.currentTimeMillis() + " }";

        try {
            ObjectMapper objectMapper = new ObjectMapper();
            Task task = objectMapper.readValue(taskJson, Task.class);
            System.out.println(task);
            return task;
        }
        catch(IOException e) {
            e.printStackTrace();
        }

        return null;
    }
}
