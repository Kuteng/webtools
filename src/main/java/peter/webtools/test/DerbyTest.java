package peter.webtools.test;
/*************************************************************************
	 File Name: DerbyTest.java
	 Author: 
	 Mail: 
	 Created Time: 2016年09月24日 星期六 02时48分27秒
 ************************************************************************/
import java.sql.*;

public class DerbyTest {
    public void test() {
        String driver = "org.apache.derby.jdbc.EmbeddedDriver";
        String connectionURL = "jdbc:derby:/home/peter/local/dbs/firstdb";
        Connection conn = null;
        PreparedStatement pstmt;
        String sql = "select * from task";

        try {
             Class.forName(driver);
            conn = DriverManager.getConnection(connectionURL);
            pstmt = (PreparedStatement) conn.prepareStatement(sql);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                System.out.println(rs.getString(2));
            }
        }
        catch(Exception e) {
            e.printStackTrace();
        }
        finally {
            if(conn != null) {
                try {
                    conn.close();
                }
                catch(Exception e) {}
            }
        }
        
    }
    
}

