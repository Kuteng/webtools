/*************************************************************************
	> File Name: Idea.java
	> Author: 
	> Mail: 
	> Created Time: 2016年10月11日 星期二 20时47分05秒
 ************************************************************************/


package peter.webtools.beans;

import java.sql.Timestamp;

/**
 * 感悟类，记录日常的一些感悟。
 */
public class Idea {
    private int id;
    private String content;
    private String tags;
    private Timestamp time;
    private int nextId;

    public Idea() {
    }

    public Idea(String content) {
        this.content = content;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return this.id;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getContent() {
        return this.content;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getTags() {
        return this.tags;
    }

    public void setTime(Timestamp time) {
        this.time = time;
    }

    public Timestamp getTime() {
        return this.time;
    }

    @Override
    public String toString() {
        return "Idea[" + id + ", " + content + ", " + time + ", " + tags + "]";
    }
}
