package peter.webtools.beans;

import java.sql.Timestamp;
import java.util.List;
import java.util.ArrayList;
import java.util.Collections;
import org.codehaus.jackson.map.ObjectMapper;
import java.io.IOException;

/**
 * 任务类，纪录任务信息。
 */
public class Task {
    private int id;
    private String name;
    private int classId;
    private String className;
    private List<String> tags;
    private String details;
    private Timestamp startTime;
    private Timestamp endTime;
    private int preId;

    public Task() {
    }

    public Task(String name) {
        this.name = name;
    }

    public Task(int id, String name) {
        this.name = name;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return this.id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public void setClassId(Integer classId) {
        if(classId == null) {
            classId = 0;
        }

        this.classId = classId;
    }

    public int getClassId() {
        return this.classId;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getClassName() {
        return this.className;
    }

    public void setTagsByArray(String[] arg) {
        if(arg == null || arg.length == 0) {
            this.tags = null;
        }

        this.tags = new ArrayList<String>();
        Collections.addAll(this.tags, arg);
    }

    // @temp by peter: 此方法需要验证，同时需要将其抽到工具方法中。将json字符串转化为List对象。
    public void setTagsByString(String str) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            ArrayList<String> tags = objectMapper.readValue(str, ArrayList.class);
            this.tags = tags;
        }
        catch(IOException e) {
            e.printStackTrace();
        }
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public List<String> getTags() {
        return this.tags;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getDetails() {
        return this.details;
    }

    public void setStartTime(Timestamp startTime) {
        this.startTime = startTime;
    }

    public Timestamp getStartTime() {
        return this.startTime;
    }

    public void setEndTime(Timestamp endTime) {
        this.endTime = endTime;
    }

    public Timestamp getEndTime() {
        return this.endTime;
    }

    public int getPreId() {
        return this.preId;
    }

    public void setPreId(int preId) {
        this.preId = preId;
    }

    @Override
    public String toString() {
        return "Task[" + id + ", " + name + ", " + startTime + ", " + endTime + "]";
        // return String.format("Task[id: %d, name: %s, startTime: %s, endTime: %s, nextId: %d]", this.id, this.name, this.startTime.toString(), this.endTime.toString());
    }

    // getters & setters omitted for brevity
}
