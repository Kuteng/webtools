#spring.datasource.url=jdbc:derby://localhost:1527/firstdb
# spring.datasource.url=jdbc:derby://192.168.199.135:1527/firstdb
# spring.datasource.url=jdbc:derby://localhost:3306/test
# spring.datasource.username=user
# spring.datasource.password=password
# spring.datasource.schema=app
spring.datasource.url=jdbc:mysql://localhost:3306/webtools?useUnicode=true&characterEncoding=utf8
spring.datasource.driverClassName=com.mysql.jdbc.Driver
# spring.datasource.driverClassName=org.apache.derby.jdbc.ClientDriver
# pring.datasource.initialize=false
# spring.datasource.driverClassName=org.apache.derby.jdbc.EmbeddedDriver
# spring.datasource.max-active=20
# spring.datasource.max-idle=8
# spring.datasource.min-idle=8
# spring.datasource.initial-size=10
spring.mvc.favicon.enabled=false
logging.path=/home/peter/Documents/pgits/webtools/logs
