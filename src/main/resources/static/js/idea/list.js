var ideaTemplate = "<span data_id=\"%s\" class=\"idea\">%s<span class=\"elementBox\"><span class=\"tags element\">%s</span><span class=\"time element\">%s</span><span class=\"btns\"><button class=\"editBtn iconBtn element\"/><button class=\"delBtn iconBtn element\"/></span></span></span>";

$(function() {
    test();
});
// var vm = test();

function test() {
    return new Vue({
        el: "#app",
        data: {
            ideas: null,
        },
        computed: {
        },
        ready: function() {
            this.buildIdeas(0);
        },
        methods: {
            peter: function() {
                console.log(">>> peter.action");
            },
            buildIdeas: function(page) {
                this.$http.get(window.location.origin + "/idea/list", {params: {page: page}}).then(response => {
                    this.ideas = response.body;
                }, response => {
                });
            },
            changePage: function(current) {
                this.buildIdeas(current);
            },
        },
    });
}

function loadList() {
    $.ajax({
        url: "/idea/list",
        type: "post",
        data: {
            page: 0
        },
        timeout: 10000,
        dataType: "json",
        error: function(e) {
            console.log("失败");
        },
        done: function(e) {
            console.log(e);
        },
        success: function(data) {
            for(var idx in data) {
                var idea = data[idx];
                var date = new Date(idea.time);
                date = date.getUTCFullYear() + "-" + date.getUTCMonth() + "-" + date.getUTCDate() + " " + date.getUTCHours() + ":" + date.getUTCMinutes() + ":" + date.getUTCSeconds();
                var span = ideaTemplate.replace(/\%s/, idea.id).replace(/\%s/, idea.content).replace(/\%s/, idea.tags).replace(/\%s/, date);
                $("#idealist").append(span);
            }
        }
    })
}
