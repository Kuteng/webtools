function ready() {
    Vue.component('idea-item', Vue.extend({
        props: ['idea'],
        template: '\
            <span :data-id="idea.id" class="idea">\
                {{idea.content}}\
                <span class="elementBox">\
                    <span class="tags element">{{buildTags()}}</span>\
                    <span class="time element">{{buildTime()}}</span>\
                    <span class="btns">\
                        <button class="editBtn iconBtn element"/>\
                        <button class="delBtn iconBtn element"/>\
                    </span>\
                </span>\
            </span>',
        data: function() {
            return {
            };
        },
        methods: {
            buildTags: function() {
                return this.idea.tags;
            },
            buildTime: function() {
                var date = new Date(this.idea.time);
                date = date.getUTCFullYear() + "-" + date.getUTCMonth() + "-" + date.getUTCDate() + " " + date.getUTCHours() + ":" + date.getUTCMinutes() + ":" + date.getUTCSeconds();
                return date;
            },
        },
    }));
}

ready();
