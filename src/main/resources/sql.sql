-- 此内容只适用于mysql。
CREATE SCHEMA `webtools` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;

drop table webtools.accounts;
drop table webtools.apps;
create table webtools.apps(
    id int primary key auto_increment comment "id自动增长",
    name varchar(50) not null comment "网站或应用的名称。",
    url varchar(128) comment "网站的URL；应用的路径或启动命令等",
    description varchar(256) comment "应用或网站的描述"
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

create table webtools.accounts(
    id int primary key auto_increment comment "id自动增长",
    app int not null comment "app或网站的id",
    username varchar(50) not null comment "用户名，登陆是会用到的名称，注意不是用户昵称！",
    password varchar(128) not null comment "密码，此密码为加密后的",
    message varchar(256) comment "放置其他信息，如昵称（nickname）等，注册邮箱等。",
    CONSTRAINT FK_app_id_to_app FOREIGN KEY (app) references apps(id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

create table webtools.ideas (
	id int primary key auto_increment,
    content text,
    tags varchar(128),
    time timestamp DEFAULT current_timestamp
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

drop table webtools.tasks;
drop table webtools.task_class;
create table webtools.task_class (
	id int primary key auto_increment comment '任务的ID',
    name varchar(128) not null comment '任务的名称'
)engine=InnoDB default charset=utf8 comment='任务的类型，主要值大的类型，如：工作、休闲、读书等。';

create table webtools.tasks (
	id int primary key auto_increment comment '任务的ID',
    name varchar(128) not null comment '任务的名称',
    class int(2) comment '组，描述任务类型的较大单位',
    tags varchar(128) comment '标签，描述任务类型的较小单位。',
    details varchar(1024) comment '标签，描述任务类的简要。',
    start_time timestamp default current_timestamp comment '标签，任务的开始时间。',
    end_time timestamp null comment '标签，任务的结束时间。',
    pre_id int comment '上一个任务的id',
    CONSTRAINT FK_pre_id_to_task_id FOREIGN KEY (pre_id) references tasks(id),
    CONSTRAINT FK_class_id_to_task_class FOREIGN KEY (class) references task_class(id)
)engine=InnoDB default charset=utf8 comment='描述任务具体内容的表，主要用于追踪任务的开始时间与结束时间，统计个人工作效率';
