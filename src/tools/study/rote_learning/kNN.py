#!/usr/bin/env python
# coding=utf-8

from numpy import *;
import operator; # 运算符模块

def createDataSet():
    group = array([[1.0, 1.1], [1.0, 1.0], [0, 0], [0, 0.1]]);
    labels = ['A', 'A', 'B', 'B'];
    return group, labels;

def classify0(inX, dataSet, labels, k):
    dataSetSize = dataSet.shape[0];
    diffMat = tile(inX, (dataSetSize, 1)) - dataSet;
