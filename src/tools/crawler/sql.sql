-- The SQL is for mysql. --
CREATE SCHEMA `crawler` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
CREATE USER 'app'@'localhost' IDENTIFIED BY 'password';
GRANT all ON crawler.* TO 'app'@'localhost';

drop table images;
drop table pages;
create table pages (
	id int primary key auto_increment,
    code int not null unique comment '本页面的code',
    name varchar(256) comment '本页面的名称',
    url varchar(128) comment '本页面的URL',
    path varchar(128) comment '本页面的图片所在的文件夹路径',
    hasCrawler boolean default false comment '此页面信息是否已经抓取成功'
)ENGINE=InnoDB DEFAULT CHARSET=utf8;
create table images (
	id int primary key auto_increment,
    name varchar(20) comment '图片的名字',
    page_id int comment '图片所在页面的ID',
    url varchar(256) comment '图片的URL',
    hasDownload boolean default false comment '标示此图片是否已经被下载',
    CONSTRAINT FK_page_id_to_page FOREIGN KEY (page_id) references pages(id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;
